package io.booktrove.app.repository.model

import jakarta.persistence.Column
import jakarta.persistence.Entity
import jakarta.persistence.GeneratedValue
import jakarta.persistence.GenerationType
import jakarta.persistence.Id

@Entity
open class Author {

    @Column(name = "author_first_name", nullable = false)
    open var firstName: String? = null

    @Column(name = "author_last_name", nullable = false)
    open var lastName: String? = null

    @Id
    @Column(name = "author_id", nullable = false)
    @GeneratedValue(strategy = GenerationType.AUTO)
    open var id: Long? = null
}

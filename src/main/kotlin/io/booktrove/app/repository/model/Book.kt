package io.booktrove.app.repository.model

import jakarta.persistence.Column
import jakarta.persistence.Entity
import jakarta.persistence.GeneratedValue
import jakarta.persistence.GenerationType
import jakarta.persistence.Id
import jakarta.persistence.JoinColumn
import jakarta.persistence.ManyToOne
import jakarta.persistence.Table

@Entity
open class Book {

    @Column(name = "book_title", nullable = false)
    open var title: String? = null

    @Column(name = "book_series")
    open var series: String? = null

    @ManyToOne
    @JoinColumn(
        name = "author_id",
        nullable = false, updatable = false
    )
    open var author: Author? = null

    @Id
    @Column(name = "book_id", nullable = false)
    @GeneratedValue(strategy = GenerationType.AUTO)
    open var id: Long? = null
}
package io.booktrove.app.repository

import io.booktrove.app.repository.model.Book
import org.springframework.data.jpa.repository.JpaRepository


interface BookRepository : JpaRepository<Book, Long>
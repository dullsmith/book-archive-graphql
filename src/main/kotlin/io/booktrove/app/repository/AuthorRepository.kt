package io.booktrove.app.repository

import io.booktrove.app.repository.model.Author
import org.springframework.data.jpa.repository.JpaRepository


interface AuthorRepository : JpaRepository<Author, Long>
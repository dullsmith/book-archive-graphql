package io.booktrove.app.api.controller.rest

import io.booktrove.app.service.BookService
import io.booktrove.app.repository.model.Book
import io.swagger.v3.oas.annotations.Operation
import io.swagger.v3.oas.annotations.responses.ApiResponse
import io.swagger.v3.oas.annotations.tags.Tag
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController

@RestController
@RequestMapping("/v1")
@ApiResponse(responseCode = "500", description = "Internal server error, this should not happen")
@Tag(name = "Book Controller", description = "Book endpoint")
class BookController(
    private val bookService: BookService
) {

    @Operation(
        method = "GET",
        summary = "Returns all books",
    )
    @ApiResponse(
        responseCode = "200", description = "Successful returned"
    )
    @GetMapping("all-books")
    fun getBooks(): List<Book> {
        return bookService.getAllBooks()
    }

    @Operation(
        method = "GET",
        summary = "Returns book by id",
    )
    @ApiResponse(
        responseCode = "200", description = "Successful returned"
    )
    @GetMapping("book/{id}")
    fun getBooks(@PathVariable id: Long): Book {
        return bookService.getBook(id)
    }
}

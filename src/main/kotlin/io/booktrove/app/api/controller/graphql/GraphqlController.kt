package io.booktrove.app.api.controller.graphql

import io.booktrove.app.repository.model.Author
import io.booktrove.app.repository.model.Book
import io.booktrove.app.service.AuthorService
import io.booktrove.app.service.BookService
import org.springframework.graphql.data.method.annotation.Argument
import org.springframework.graphql.data.method.annotation.MutationMapping
import org.springframework.graphql.data.method.annotation.QueryMapping
import org.springframework.graphql.data.method.annotation.SchemaMapping
import org.springframework.stereotype.Controller


@Controller
class GraphqlController(
    private val bookService: BookService,
    private val authorService: AuthorService
) {


    @QueryMapping
    fun findAllAuthors(): List<Author> {
        return authorService.getAllAuthors()
    }

    @QueryMapping
    fun authorById(@Argument id: Long): Author {
        return authorService.getAuthorById(id)
    }

    @MutationMapping
    fun newAuthor(@Argument firstName: String, @Argument lastName: String): Author {
        return authorService.createAuthor(firstName, lastName)
    }

    @QueryMapping
    fun findAllBooks(): List<Book> {
        return bookService.getAllBooks()
    }

    @QueryMapping
    fun bookById(@Argument id: Long): List<Book> {
        return bookService.getAllBooks()
    }

    @SchemaMapping
    fun author(book: Book): Author? {
        return book.id?.let { authorService.getAuthorById(it) }
    }
}
package io.booktrove.app.service

import io.booktrove.app.repository.model.Book

interface BookService {
    fun getBook(id: Long): Book
    fun getAllBooks(): List<Book>
}
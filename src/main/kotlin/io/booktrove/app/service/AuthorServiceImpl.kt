package io.booktrove.app.service

import io.booktrove.app.repository.AuthorRepository
import io.booktrove.app.repository.model.Author
import org.springframework.stereotype.Service

@Service
class AuthorServiceImpl(
    private val authorRepository: AuthorRepository
) : AuthorService {
    override fun getAllAuthors(): List<Author> {
        return authorRepository.findAll()
    }

    override fun getAuthorById(id: Long): Author {
        return authorRepository.findById(id).get()
    }

    override fun createAuthor(firstName: String, lastName: String): Author {
        val author = Author()
        author.firstName = firstName
        author.lastName = lastName
        return authorRepository.save(author)
    }
}
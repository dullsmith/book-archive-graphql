package io.booktrove.app.service

import io.booktrove.app.repository.BookRepository
import io.booktrove.app.repository.model.Book
import org.springframework.stereotype.Service

@Service
class BookServiceImpl(
    private val bookRepository: BookRepository
) : BookService {
    override fun getBook(id: Long): Book {
        return bookRepository.findById(id).get()
    }

    override fun getAllBooks(): List<Book> {
        return bookRepository.findAll()
    }
}
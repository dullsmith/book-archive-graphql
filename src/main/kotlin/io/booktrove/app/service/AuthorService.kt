package io.booktrove.app.service

import io.booktrove.app.repository.model.Author

interface AuthorService {

    fun getAllAuthors(): List<Author>
    fun getAuthorById(id: Long): Author
    fun createAuthor(firstName: String, lastName: String): Author

}

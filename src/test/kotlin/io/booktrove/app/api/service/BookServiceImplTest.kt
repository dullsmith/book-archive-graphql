package io.booktrove.app.api.service

import io.booktrove.app.service.BookServiceImpl
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.extension.ExtendWith
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.test.context.junit.jupiter.SpringExtension

@SpringBootTest
@ExtendWith(SpringExtension::class)
class BookServiceImplTest(
    @Autowired private val service: BookServiceImpl
) {
    @Test
    fun `get expected response`() {
        //when
        val result = service.getAllBooks()
        //then
        assertEquals(result, "some response!")
    }
}
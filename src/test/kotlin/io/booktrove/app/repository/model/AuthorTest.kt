//package io.booktrove.app.repository.model
//
//import org.junit.jupiter.api.Assertions.*
//import org.junit.jupiter.api.Test
//
//class AuthorTest{
//
//    @Test
//    fun givenPerson_whenSaved_thenFound() {
//        doInHibernate(({ this.sessionFactory() }), { session ->
//            val personToSave = Author("John")
//            session.persist(personToSave)
//            val personFound = session.find(Author::class.java, personToSave.id)
//            session.refresh(personFound)
//
//            assertTrue(personToSave.name == personFound.name)
//        })
//    }
//}
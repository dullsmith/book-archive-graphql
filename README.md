# Book Trove

A dummy application to play around with graphql

## TODO

* how to use curl to query graphql
* how to test graphql
* how to create multiple authors at the same time
* how to create a book and author at the same time

## Endpoints

* http://localhost:8080/graphiql
* http://localhost:8080/swagger-ui

## WIP - not working: curl
curl -g \
-X POST \
-H "Content-Type: application/json" \
-d '{"query":"query{ findAllAuthors{ firstName, lastName, id }}"}' \
http://localhost:8080


## Query

```
query{
  findAllAuthors{
    firstName,
    lastName,
    id
  }
}
```

## Mutation

```
mutation {
  newAuthor(firstName: "China", lastName: "Mieville") {
    firstName
  }
}
```

## References

* https://github.com/ElektronPlus/sanavaulth-api/tree/main
* https://github.com/mckernant1/lol-api-service/tree/main
* https://graphql.com/learn/
* https://www.youtube.com/watch?v=atA2OovQBic
* https://www.youtube.com/watch?v=KYNR5js2cXE